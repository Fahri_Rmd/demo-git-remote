<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    echo "<h1>Latihan Merubah Huruf No.2</h1>";
    function ubah_huruf($string){
        $Alphabet="abcdefghijklmnopqrstuvwxyz";
        $penerima="";
        for($i = 0; $i < strlen($string); $i++){
            $position = strpos($Alphabet, $string[$i]);
            $penerima .= substr($Alphabet, $position +1,1);
        }
        return $penerima . "<br>";
    }
    
    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu
    
    

    ?>
</body>
</html>